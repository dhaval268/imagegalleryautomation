package qaf.example.tests;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import qaf.example.steps.ImageGallaryTestPage;

public class ImageGallery extends WebDriverTestCase {

	ImageGallaryTestPage gallery = new ImageGallaryTestPage();

	@Test(priority = 1)
	public void imageGalleryDemo() {

		gallery.launchPage();
		gallery.verifySiteIsLaunched();
		Reporter.log("Site is launched");
		gallery.uploadImages("Chrysanthemum.jpg","Penguins.jpg");
		Reporter.log("Images are uploaded");
		gallery.openImageOneByOne();
		gallery.verifyUploadedImageInPreview();
		gallery.deleteAllUploadedImages();
		gallery.tearDown();

	}

}
