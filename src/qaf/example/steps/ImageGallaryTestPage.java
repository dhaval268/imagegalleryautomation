package qaf.example.steps;

import static org.testng.Assert.assertEquals;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ImageGallaryTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "imagegallery.header")
	private QAFWebElement imageGalleryHeader;

	@FindBy(locator = "imagegallery.uploadfilebutton")
	private QAFWebElement uploadFileButton;

	//@FindBy(locator = "imagegallery.thumbnailList")
	//private List<QAFWebElement> uploadedImageList;

	@FindBy(locator = "imagegallery.closepreview")
	private QAFWebElement closePreview;

	@FindBy(locator = "imagegallery.timeinpreview")
	private QAFWebElement timeInPreivew;

	@FindBy(locator = "imagegallery.hiddenprivew")
	private QAFWebElement previewClosed;

	@FindBy(locator = "imagegallery.deleteImage")
	private QAFWebElement deleteIcon;

	@FindBy(locator = "imagegallery.leftarrow")
	private QAFWebElement leftArrow;

	@FindBy(locator = "imagegallery.rightarrow")
	private QAFWebElement rightArrow;
	
	@FindBy(locator = "imagegallery.dataimagenumber")
	private QAFWebElement dataImageNumber;
	

	public List<QAFWebElement> getUploadedImageList() {
		List<QAFWebElement> uploadedImageList = driver.findElements("imagegallery.thumbnailList");
		return uploadedImageList;
	}
	
	public QAFWebElement getDesiredImageElement(String imageSrc) {
		String imageSources = String.format(ConfigurationManager.getBundle().getString("imagegallery.imageLink"),
				imageSrc.trim());
		QAFExtendedWebElement imageLink = new QAFExtendedWebElement(imageSources);
		return imageLink;
	}

	Alert deleteAlert;
	String deleteMessage = "Delete this image?";
	//List<String> imageSource = new LinkedList<>();
	private static final java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		try {
			driver.get("/");
			driver.manage().window().maximize();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@QAFTestStep(description = "launch browser and url")
	public void launchPage() {
		openPage(null, null);
	}

	/**
	 * Wait for Image gallery and upload button displayed
	 */
	@QAFTestStep(description = "Verify site is launched")
	public void verifySiteIsLaunched() {
		imageGalleryHeader.waitForPresent();
		imageGalleryHeader.waitForVisible();
		uploadFileButton.waitForPresent();
		//uploadFileButton.waitForVisible();
	}

	/**
	 * Upload multiple images
	 */
	public void uploadImages(String... images) {
		System.out.println(images);
		for (String image : images) {
			uploadImage(image);
		}
	}
	
	/**
	 * Upload multiple images
	 */
	@QAFTestStep(description = "Upload images {0}")
	public void uploadImages(String imageNames) {
		String[] images =  imageNames.split(",");
		System.out.println(images);
		for (String image : images) {
			uploadImage(image);
		}
	}

	/**
	 * 
	 * This method is used to upload single image file
	 * 
	 */
	@QAFTestStep(description = "upload single image file")
	public void uploadImage(String fileName) {
		int countBeforeUpload = uploadedImageCount();
		String currentDir = System.getProperty("user.dir");
		uploadFileButton.sendKeys(currentDir + "//resources//images//" + fileName.trim());
		implicitWait(3000);
		int countAfterUpload = uploadedImageCount();
		System.out.println((countBeforeUpload + 1) +" "+ countAfterUpload);
		assertEquals(countBeforeUpload + 1, countAfterUpload);
	}

	/**
	 * return : Uploaded image count
	 * 
	 * get count of uploaded images
	 */
	public int uploadedImageCount() {
		
		try {
			return getUploadedImageList().size();
		}
		catch (Exception e) {
			return 0;
		}
	}

	/**
	 * open images one by one
	 */
	@QAFTestStep(description = "open images one by one")
	public void openImageOneByOne() {

		for (QAFWebElement image : getUploadedImageList()) {
			image.click();
			String preivewDate = timeInPreivew.getText().trim();
			java.util.Date imageDate;
			try {
				imageDate = sdf.parse(preivewDate.trim());
				assertEquals(sdf.format(imageDate), preivewDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			closePreview.click();
			previewClosed.waitForPresent();
			previewClosed.verifyPresent();
		}
	}

	/**
	 * Delete images
	 */
	@QAFTestStep(description = "Delete all uploaded images")
	public void deleteAllUploadedImages() {

		for (QAFWebElement image : getUploadedImageList()) {
			image.click();
			deleteOperation();
			cancelDelete();

			deleteOperation();
			confirmDelete();
			previewClosed.waitForPresent();
			previewClosed.verifyPresent();
		}
		
		implicitWait(1500);
	}

	/**
	 * Click on delete icon in preview
	 */
	public void deleteOperation() {
		deleteIcon.click();
		deleteAlert = driver.switchTo().alert();
	}

	/**
	 * Confirm delete operation
	 */
	public void confirmDelete() {
		assertEquals(deleteAlert.getText().trim(), deleteMessage);
		deleteAlert.accept();
	}

	/**
	 * Cancel delete operation
	 */
	public void cancelDelete() {
		assertEquals(deleteAlert.getText().trim(), deleteMessage);
		deleteAlert.dismiss();
	}
	
	@QAFTestStep(description = "Verify image in preview")
	public void verifyUploadedImageInPreview() {
		getUploadedImageList().get(0).click();
		
		for(int i = 0 ;i< getUploadedImageList().size() ; i++) {
			int currentDataImageNumber = Integer.parseInt(dataImageNumber.getAttribute("data-image"));
			assertEquals(i+1, currentDataImageNumber);
			rightArrow.click();
		}
		
		for(int i = getUploadedImageList().size() ;i>0 ; i--) {
			leftArrow.click();
			int currentDataImageNumber = Integer.parseInt(dataImageNumber.getAttribute("data-image"));
			assertEquals(i, currentDataImageNumber);
		}
		
		closePreview.click();
		previewClosed.waitForPresent();
		previewClosed.verifyPresent();
	}

	/**
	 * Click on left navigation arrow
	 */
	public void clickOnLeftArrow() {
		leftArrow.click();
	}

	/**
	 * Click on right navigation arrow
	 */
	public void clickOnRightArrow() {
		rightArrow.click();
	}
	
	public void implicitWait(int timeSeconds)
	{
		driver.manage().timeouts().implicitlyWait(timeSeconds,TimeUnit.SECONDS) ;
	}

	@QAFTestStep(description = "close the browser")
	public void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}
	
	

}
