package qaf.example.listener;

import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.Response;
import org.testng.annotations.AfterClass;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.webdriver.CommandTracker;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriverCommandAdapter;
import com.qmetry.qaf.automation.util.StringUtil;

public class WDListener extends QAFWebDriverCommandAdapter {
    Log logger = LogFactory.getLog(getClass());
    int counter = 0;
    @Override
    public void beforeCommand(QAFExtendedWebDriver driver, CommandTracker commandTracker) {
        super.beforeCommand(driver, commandTracker);
        String command = commandTracker.getCommand();
        Map<String, Object> params = commandTracker.getParameters();
        // support selenium 1 api for navigating from frame to main window
        // with selenium.selectFrame("");
        if (command.equalsIgnoreCase(DriverCommand.GET) ) {
            
        	if(counter==0) {
        		driver.manage().window().maximize();
        	}
        	counter ++;
        }
    }
    @AfterClass
    public void resetCounter() {
    	counter=0;
    }
}
